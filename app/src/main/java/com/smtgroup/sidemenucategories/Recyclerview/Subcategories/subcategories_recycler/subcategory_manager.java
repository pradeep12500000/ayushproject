package com.smtgroup.sidemenucategories.Recyclerview.Subcategories.subcategories_recycler;

import java.util.ArrayList;

public class subcategory_manager {

    public ArrayList<subcategory_model> gtesubcategory_models(){
        ArrayList<subcategory_model> subcategory_models = new ArrayList<>();
        subcategory_models.add(new subcategory_model("nokia"));
        subcategory_models.add(new subcategory_model("appo"));
        subcategory_models.add(new subcategory_model("vivo"));
        subcategory_models.add(new subcategory_model("samsung"));
        subcategory_models.add(new subcategory_model("motorola"));
        subcategory_models.add(new subcategory_model("honor"));
        return subcategory_models;
    }
}
