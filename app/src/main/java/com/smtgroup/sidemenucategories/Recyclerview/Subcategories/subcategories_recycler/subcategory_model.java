package com.smtgroup.sidemenucategories.Recyclerview.Subcategories.subcategories_recycler;

public class subcategory_model {
    String subcategory ;

    public String getSubcategory() {
        return subcategory;
    }

    public void setSubcategory(String subcategory) {
        this.subcategory = subcategory;
    }

    public subcategory_model(String subcategory) {

        this.subcategory = subcategory;
    }
}
