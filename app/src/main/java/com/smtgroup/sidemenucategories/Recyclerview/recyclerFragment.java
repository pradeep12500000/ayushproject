package com.smtgroup.sidemenucategories.Recyclerview;


import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.smtgroup.sidemenucategories.R;
import com.smtgroup.sidemenucategories.Recyclerview.adapter.adapter;

import com.smtgroup.sidemenucategories.Recyclerview.model.model;
import com.smtgroup.sidemenucategories.database.dbhelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class recyclerFragment extends Fragment {

    public Context context;
    public View view;
    dbhelper mydb;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    Unbinder unbinder;
    ArrayList<model> arrayList;

    public recyclerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_recycler, container, false);
        unbinder = ButterKnife.bind(this, view);
       arrayList = new ArrayList<>();


        Cursor cursor = mydb.viewdata();
        if (cursor.getCount() == 0){
            Toast.makeText(context,"enter category",Toast.LENGTH_SHORT).show();
        } else {
            model model = new model(cursor.getString(1));
            arrayList.add(model);
        }
        adapter adapter;
        adapter = new adapter(context,arrayList);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context,1);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setAdapter(adapter);

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}
