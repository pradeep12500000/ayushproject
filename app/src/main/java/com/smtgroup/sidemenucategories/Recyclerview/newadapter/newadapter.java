package com.smtgroup.sidemenucategories.Recyclerview.newadapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.smtgroup.sidemenucategories.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class newadapter extends RecyclerView.Adapter<newadapter.Viewholder> {

    Context context;
    ArrayList<newmodel> list;
    onclick click;


    public newadapter(Context context, ArrayList<newmodel> list,onclick click) {
        this.context = context;
        this.list = list;
        this.click = click;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_recycler1, viewGroup, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder viewholder, int position) {
        viewholder.text.setText(list.get(position).getProd());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }



    public class Viewholder extends RecyclerView.ViewHolder {
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.onclick)
        RelativeLayout onclick;
        @OnClick(R.id.text)
        public void onViewClicked() {
            click.oncategoryclick(getAdapterPosition());
            }
            public Viewholder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
