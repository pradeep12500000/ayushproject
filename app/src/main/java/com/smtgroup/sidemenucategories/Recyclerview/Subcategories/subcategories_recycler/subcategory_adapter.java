package com.smtgroup.sidemenucategories.Recyclerview.Subcategories.subcategories_recycler;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.smtgroup.sidemenucategories.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class subcategory_adapter extends RecyclerView.Adapter<subcategory_adapter.ViewHolder> {

    onclick onclick;
    public ArrayList<subcategory_model> arrayList;
    public Context context;


    public subcategory_adapter(ArrayList<subcategory_model> arrayList, Context context, onclick onclick) {
        this.arrayList = arrayList;
        this.context = context;
        this.onclick = onclick;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(context).inflate(R.layout.row_recycler_subcategory, viewGroup, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewholder, int i) {
        viewholder.subcategory.setText(arrayList.get(i).getSubcategory());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.subcategory)
        TextView subcategory;
        @OnClick(R.id.subcategory)
        public void onViewClicked() {
            onclick.onsubcategoryclick(getAdapterPosition());
        }
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
