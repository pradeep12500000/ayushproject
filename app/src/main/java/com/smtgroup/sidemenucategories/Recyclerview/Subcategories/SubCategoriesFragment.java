package com.smtgroup.sidemenucategories.Recyclerview.Subcategories;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.smtgroup.sidemenucategories.R;
import com.smtgroup.sidemenucategories.Recyclerview.adapter.adapter;
import com.smtgroup.sidemenucategories.database.dbhelper;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class SubCategoriesFragment extends Fragment {

    public Context context;
    public View view;
    @BindView(R.id.spinner3)
    Spinner spinner3;
    @BindView(R.id.submit)
    Button submit;
    Unbinder unbinder;
    dbhelper dbhelper;
    List<String> categories=new ArrayList<>();

    @BindView(R.id.EnterSubCategories)
    EditText EnterSubCategories;



    public SubCategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_sub_categories, container, false);
        ButterKnife.bind(this, view);
        dbhelper = new dbhelper(context);
        categories = dbhelper.getAllUsers();
        ArrayAdapter abcd = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1, categories);
        abcd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner3.setAdapter(abcd);
        return view;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @OnClick(R.id.submit)
    public void onViewClicked() {
        Toast.makeText(getContext(), EnterSubCategories.getText().toString() + " is selected as subcategory", Toast.LENGTH_SHORT).show();

    }
}
