package com.smtgroup.sidemenucategories.Recyclerview.Subcategories.subcategories_recycler;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.smtgroup.sidemenucategories.R;
import com.smtgroup.sidemenucategories.Recyclerview.recyclerFragment;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * A simple {@link Fragment} subclass.
 */
public class recyclerSubcategory_Fragment extends Fragment implements onclick {

    public Context context;
    public View view;
    public ArrayList<subcategory_model> arrayList;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    Unbinder unbinder;
    @BindView(R.id.subcategory)
    LinearLayout subcategory;


    public recyclerSubcategory_Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_blank, container, false);
        unbinder = ButterKnife.bind(this, view);
        arrayList = new ArrayList<>();
        subcategory_adapter subcategory_adapter = new subcategory_adapter(new subcategory_manager().gtesubcategory_models(),
                context, this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setAdapter(subcategory_adapter);
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void onsubcategoryclick(int positon) {
        replacefragement(new recyclerFragment(), true);

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void replacefragement(recyclerFragment recyclerFragment, boolean b) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_main,recyclerFragment).addToBackStack(null);
        fragmentTransaction.commit();

    }
}
