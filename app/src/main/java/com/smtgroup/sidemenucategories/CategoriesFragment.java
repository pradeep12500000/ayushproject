package com.smtgroup.sidemenucategories;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.smtgroup.sidemenucategories.Recyclerview.model.model;
import com.smtgroup.sidemenucategories.database.dbhelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class CategoriesFragment extends Fragment {

    public Context context;
    public View view;
    dbhelper mydb;
    @BindView(R.id.submit)
    Button submit;
    Unbinder unbinder;
    @BindView(R.id.edit_text)
    EditText editText;

    public CategoriesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_categories, container, false);
        unbinder = ButterKnife.bind(this, view);
        mydb = new dbhelper(context);

        return view;

    }

    public void AddData(model model){
        boolean insertData = mydb.addData(model);
        if (insertData){
            Toast.makeText(context,"data succesfully inserted",Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(context,"something went wrong",Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @OnClick(R.id.submit)
    public void onViewClicked() {
String newcategory = editText.getText().toString();
if (editText.length() !=0){
    model model = new model(newcategory);
    AddData(model);
    editText.setText("");
}
else {
    Toast.makeText(context,"please enter something",Toast.LENGTH_SHORT).show();;
}

    }

}
