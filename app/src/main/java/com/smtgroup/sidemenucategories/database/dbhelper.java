package com.smtgroup.sidemenucategories.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.smtgroup.sidemenucategories.Recyclerview.model.model;

import java.util.ArrayList;
import java.util.List;

public class dbhelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "sidemenu.db";
    public static final String TABLE_NAME = "categories";
    public static final String ID = "id";
    public static final String values = "category";



    public dbhelper( Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table "+TABLE_NAME+"(id integer primary key autoincrement,category text)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " +TABLE_NAME);
        }

        public boolean addData(model model){
        SQLiteDatabase databse = this.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(values,model.getCategories());

            long result = databse.insert(TABLE_NAME,null,contentValues);
            if (result == -1){
                return false;
            }
            else {
                return true;
            }
        }

    public List<String> getAllUsers()
    {
        List<String> userlist=new ArrayList<>();
        //get readable database
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery("select category from " +TABLE_NAME,null);
        if(cursor.moveToFirst())
        {
            do {
                userlist.add(cursor.getString(0));
            }while (cursor.moveToNext());
        }
        //close the cursor
        cursor.close();
        //close the database
        db.close();
        return userlist;
    }

        public Cursor viewdata(){
        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();
        String query = "select * from "+TABLE_NAME;
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        return cursor;
        }
}
