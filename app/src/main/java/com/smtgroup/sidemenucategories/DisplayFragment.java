package com.smtgroup.sidemenucategories;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.smtgroup.sidemenucategories.Recyclerview.Subcategories.subcategories_recycler.recyclerSubcategory_Fragment;
import com.smtgroup.sidemenucategories.Recyclerview.newadapter.newadapter;
import com.smtgroup.sidemenucategories.Recyclerview.newadapter.newmanager;
import com.smtgroup.sidemenucategories.Recyclerview.newadapter.newmodel;
import com.smtgroup.sidemenucategories.Recyclerview.newadapter.onclick;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class DisplayFragment extends Fragment implements onclick {
    public Context context;
    public View view;
    ArrayList<newmodel> displayarrayList;
    @BindView(R.id.recyclerview1)
    RecyclerView view1;
    Unbinder unbinder;
    @BindView(R.id.replace)
    RelativeLayout replace;
    @BindView(R.id.displayfragement)
    RelativeLayout displayfragement;


    public DisplayFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_display, container, false);
        ButterKnife.bind(this, view);
        displayarrayList = new ArrayList<>();
        newadapter adapter = new newadapter(context, new newmanager().getnewmodels(), this);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 2);
        view1.setLayoutManager(layoutManager);
        view1.setAdapter(adapter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void oncategoryclick(int position) {
        replaceFragmenet(new recyclerSubcategory_Fragment(), true);
    }

    private void replaceFragmenet(recyclerSubcategory_Fragment recyclerSubcategory_fragment, boolean b) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_main, recyclerSubcategory_fragment).addToBackStack(null);
        fragmentTransaction.commit();

    }


}
