package com.smtgroup.sidemenucategories;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.smtgroup.sidemenucategories.Recyclerview.recyclerFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


/**
 * A simple {@link Fragment} subclass.
 */
public class ItemFragment extends Fragment {
    @BindView(R.id.spinner1)
    Spinner spinner1;
    Unbinder unbinder;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.spinner2)
    Spinner spinner2;
    @BindView(R.id.edittext_model)
    EditText edittextModel;
    @BindView(R.id.edittext_quantity)
    EditText edittextQuantity;
    @BindView(R.id.edittext_price)
    EditText edittextPrice;
    @BindView(R.id.item_fragement)
    RelativeLayout itemFragement;
    private Context context;
    String[] items = {"mobile", "refrigerator", "laptop", "air condition", "fan"};
    String[] subcategories = {"nokia", "appo", "vivo", "motorola", "samsung", "apple"};

    public ItemFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_item, container, false);
        unbinder = ButterKnife.bind(this, view);
        ArrayAdapter abcd = new ArrayAdapter(context, android.R.layout.simple_list_item_1, items);
        abcd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        ArrayAdapter adapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, subcategories);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner2.setAdapter(adapter);

        spinner1.setAdapter(abcd);
        return view;

    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.submit)
    public void onViewClicked() {
        replacefragement(new recyclerFragment(), true);
    }

    private void replacefragement(recyclerFragment recyclerFragment, boolean b) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.item_fragement, recyclerFragment).addToBackStack(null);
        fragmentTransaction.commit();
    }


}
