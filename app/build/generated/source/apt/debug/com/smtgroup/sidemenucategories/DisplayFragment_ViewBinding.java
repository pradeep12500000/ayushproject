// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class DisplayFragment_ViewBinding implements Unbinder {
  private DisplayFragment target;

  @UiThread
  public DisplayFragment_ViewBinding(DisplayFragment target, View source) {
    this.target = target;

    target.view1 = Utils.findRequiredViewAsType(source, R.id.recyclerview1, "field 'view1'", RecyclerView.class);
    target.replace = Utils.findRequiredViewAsType(source, R.id.replace, "field 'replace'", RelativeLayout.class);
    target.displayfragement = Utils.findRequiredViewAsType(source, R.id.displayfragement, "field 'displayfragement'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    DisplayFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.view1 = null;
    target.replace = null;
    target.displayfragement = null;
  }
}
