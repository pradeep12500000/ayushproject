// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories.Recyclerview.newadapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.smtgroup.sidemenucategories.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class newadapter_ViewBinding implements Unbinder {
  private newadapter target;

  @UiThread
  public newadapter_ViewBinding(newadapter target, View source) {
    this.target = target;

    target.text = Utils.findRequiredViewAsType(source, R.id.text, "field 'text'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    newadapter target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.text = null;
  }
}
