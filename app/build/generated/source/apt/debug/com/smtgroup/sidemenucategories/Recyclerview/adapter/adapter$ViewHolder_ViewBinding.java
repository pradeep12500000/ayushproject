// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories.Recyclerview.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.smtgroup.sidemenucategories.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class adapter$ViewHolder_ViewBinding implements Unbinder {
  private adapter.ViewHolder target;

  @UiThread
  public adapter$ViewHolder_ViewBinding(adapter.ViewHolder target, View source) {
    this.target = target;

    target.categories = Utils.findRequiredViewAsType(source, R.id.categories, "field 'categories'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    adapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.categories = null;
  }
}
