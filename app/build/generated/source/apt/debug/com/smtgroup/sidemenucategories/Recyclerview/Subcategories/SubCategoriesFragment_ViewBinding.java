// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories.Recyclerview.Subcategories;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.smtgroup.sidemenucategories.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SubCategoriesFragment_ViewBinding implements Unbinder {
  private SubCategoriesFragment target;

  private View view7f0800c1;

  @UiThread
  public SubCategoriesFragment_ViewBinding(final SubCategoriesFragment target, View source) {
    this.target = target;

    View view;
    target.spinner3 = Utils.findRequiredViewAsType(source, R.id.spinner3, "field 'spinner3'", Spinner.class);
    view = Utils.findRequiredView(source, R.id.submit, "field 'submit' and method 'onViewClicked'");
    target.submit = Utils.castView(view, R.id.submit, "field 'submit'", Button.class);
    view7f0800c1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.EnterSubCategories = Utils.findRequiredViewAsType(source, R.id.EnterSubCategories, "field 'EnterSubCategories'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SubCategoriesFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.spinner3 = null;
    target.submit = null;
    target.EnterSubCategories = null;

    view7f0800c1.setOnClickListener(null);
    view7f0800c1 = null;
  }
}
