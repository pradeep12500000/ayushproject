// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories.Recyclerview.Subcategories.subcategories_recycler;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.smtgroup.sidemenucategories.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class subcategory_adapter$ViewHolder_ViewBinding implements Unbinder {
  private subcategory_adapter.ViewHolder target;

  private View view7f0800bf;

  @UiThread
  public subcategory_adapter$ViewHolder_ViewBinding(final subcategory_adapter.ViewHolder target,
      View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.subcategory, "field 'subcategory' and method 'onViewClicked'");
    target.subcategory = Utils.castView(view, R.id.subcategory, "field 'subcategory'", TextView.class);
    view7f0800bf = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    subcategory_adapter.ViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.subcategory = null;

    view7f0800bf.setOnClickListener(null);
    view7f0800bf = null;
  }
}
