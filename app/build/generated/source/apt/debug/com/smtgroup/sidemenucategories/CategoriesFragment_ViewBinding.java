// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CategoriesFragment_ViewBinding implements Unbinder {
  private CategoriesFragment target;

  private View view7f0800c1;

  @UiThread
  public CategoriesFragment_ViewBinding(final CategoriesFragment target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.submit, "field 'submit' and method 'onViewClicked'");
    target.submit = Utils.castView(view, R.id.submit, "field 'submit'", Button.class);
    view7f0800c1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.editText = Utils.findRequiredViewAsType(source, R.id.edit_text, "field 'editText'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CategoriesFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.submit = null;
    target.editText = null;

    view7f0800c1.setOnClickListener(null);
    view7f0800c1 = null;
  }
}
