// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories.Recyclerview.Subcategories.subcategories_recycler;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.smtgroup.sidemenucategories.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class recyclerSubcategory_Fragment_ViewBinding implements Unbinder {
  private recyclerSubcategory_Fragment target;

  @UiThread
  public recyclerSubcategory_Fragment_ViewBinding(recyclerSubcategory_Fragment target,
      View source) {
    this.target = target;

    target.recyclerview = Utils.findRequiredViewAsType(source, R.id.recyclerview, "field 'recyclerview'", RecyclerView.class);
    target.subcategory = Utils.findRequiredViewAsType(source, R.id.subcategory, "field 'subcategory'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    recyclerSubcategory_Fragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.recyclerview = null;
    target.subcategory = null;
  }
}
