// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories.Recyclerview.newadapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.smtgroup.sidemenucategories.R;
import java.lang.IllegalStateException;
import java.lang.Override;

public class newadapter$Viewholder_ViewBinding implements Unbinder {
  private newadapter.Viewholder target;

  private View view7f0800c7;

  @UiThread
  public newadapter$Viewholder_ViewBinding(final newadapter.Viewholder target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.text, "field 'text' and method 'onViewClicked'");
    target.text = Utils.castView(view, R.id.text, "field 'text'", TextView.class);
    view7f0800c7 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.onclick = Utils.findRequiredViewAsType(source, R.id.onclick, "field 'onclick'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    newadapter.Viewholder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.text = null;
    target.onclick = null;

    view7f0800c7.setOnClickListener(null);
    view7f0800c7 = null;
  }
}
