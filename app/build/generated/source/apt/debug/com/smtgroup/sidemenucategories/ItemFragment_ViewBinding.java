// Generated code from Butter Knife. Do not modify!
package com.smtgroup.sidemenucategories;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ItemFragment_ViewBinding implements Unbinder {
  private ItemFragment target;

  private View view7f0800c1;

  @UiThread
  public ItemFragment_ViewBinding(final ItemFragment target, View source) {
    this.target = target;

    View view;
    target.spinner1 = Utils.findRequiredViewAsType(source, R.id.spinner1, "field 'spinner1'", Spinner.class);
    view = Utils.findRequiredView(source, R.id.submit, "field 'submit' and method 'onViewClicked'");
    target.submit = Utils.castView(view, R.id.submit, "field 'submit'", Button.class);
    view7f0800c1 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onViewClicked();
      }
    });
    target.spinner2 = Utils.findRequiredViewAsType(source, R.id.spinner2, "field 'spinner2'", Spinner.class);
    target.edittextModel = Utils.findRequiredViewAsType(source, R.id.edittext_model, "field 'edittextModel'", EditText.class);
    target.edittextQuantity = Utils.findRequiredViewAsType(source, R.id.edittext_quantity, "field 'edittextQuantity'", EditText.class);
    target.edittextPrice = Utils.findRequiredViewAsType(source, R.id.edittext_price, "field 'edittextPrice'", EditText.class);
    target.itemFragement = Utils.findRequiredViewAsType(source, R.id.item_fragement, "field 'itemFragement'", RelativeLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ItemFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.spinner1 = null;
    target.submit = null;
    target.spinner2 = null;
    target.edittextModel = null;
    target.edittextQuantity = null;
    target.edittextPrice = null;
    target.itemFragement = null;

    view7f0800c1.setOnClickListener(null);
    view7f0800c1 = null;
  }
}
